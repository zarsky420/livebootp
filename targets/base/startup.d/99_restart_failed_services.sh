
# Try to restart all failed services
for s in $(systemctl list-units --state=failed \
    | awk '
        /^  UNIT / {
            inner_list=1;
            next;
        }
        /^$/ && inner_list==1 {
            exit;
        }
        (inner_list==1) {
            print $2;
        }'); do
    systemctl restart $s || true &
done
